import java.util.ArrayList;
import java.util.List;

import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.collections.Predicate;

public class CollectionTestFilter{

	/**
	 * @param args
	 */
	
	public static void main(String args[]){
		Predicate pred = new Predicate() {
			
			@Override
			public boolean evaluate(Object object) {
				// TODO Auto-generated method stub
				String prefix = "prefix";
				if(object instanceof String){
					String str = (String)object;
					if(str.startsWith(prefix)){
						return true;
					}
				}
				return false;
			}
		};
		
		List<String> strings = new ArrayList<String>();
		strings.add("this");
		strings.add("prefixthis");
		strings.add("thisisprefix");
		strings.add("prefixthiswaht");
		strings.add("prefix");
		strings.add("prefixsomewhat");
		
		CollectionUtils.filter(strings, pred);
		
		for(int i=0; i<strings.size(); i++){
			System.out.println(strings.get(i));
		}
	}

}
