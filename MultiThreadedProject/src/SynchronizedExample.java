
class Callme{
	void callme(String msg){
		synchronized (this) {
			System.out.print("["+msg);
			try{
				Thread.sleep(1000);
			}catch(InterruptedException e){
				e.printStackTrace();
			}
			System.out.println("]");
		}
	}
}

public class SynchronizedExample implements Runnable{

	/**
	 * @param args
	 */
	String msg;
	Callme target;
	
	public SynchronizedExample(Callme target, String s){
		msg = s;
		this.target = target;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Callme c1 = new Callme();
		Thread e1 = new Thread(new SynchronizedExample(c1, "Hello"));
		Thread e2 = new Thread(new SynchronizedExample(c1, "Synchronized"));
		Thread e3 = new Thread(new SynchronizedExample(c1, "World"));

		try {
			e1.start();
			e2.start();
			e3.start();
			e1.setPriority(Thread.MAX_PRIORITY);
			e3.setPriority(Thread.MIN_PRIORITY);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		target.callme(msg);
	}

}
