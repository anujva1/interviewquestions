
public class MainClass  implements Runnable{

	/**
	 * @param args
	 */
	Thread t;
	
	MainClass(String name){
		t = new Thread(this, name);
		System.out.println("Child thread: "+t);
		t.start();//start the thread
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MainClass m1 = new MainClass("One");
		MainClass m2 = new MainClass("Two");
		MainClass m3 = new MainClass("Three");
		Thread t = Thread.currentThread();
		t.setName("My Thread");
		t.setPriority(3);
		System.out.println("Current thread: "+ t);
		try {
			m1.t.join();
			m2.t.join();
			m3.t.join();
		} catch (InterruptedException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		try{
			for(int i=0; i<5; i++){
				System.out.println(5-i);
				Thread.sleep(1000);
			}
		}catch(InterruptedException e){
			System.out.println("Main thread interrupted");
		}
		System.out.println("Main thread exiting");
	}

	@Override
	public void run() {
		// TODO Auto-generated method stub
		try{
			for(int i=0; i<5; i++){
				System.out.println("The name of the thread is: "+ this.t.getName()+" "+(5-i));
				Thread.sleep(1000);
			}
		}catch(InterruptedException e){
			System.out.println("Main thread interrupted");
		}
		System.out.println("Exiting child thread");
	}

}
