
public class QuickUnionUF {

	/**
	 * @param args
	 */
	
	private int[] id;
	
	public QuickUnionUF(int N){
		id = new int[N];
		for(int i=0; i<N; i++){
			id[i] = i;
		}
	}
	
	private int root(int p){
		while(p != id[p])
			p = id[p];
		return p;
	}
	
	public boolean connected(int p, int q){
		if(root(p) == root(q)){
			return true;
		}else
			return false;
	}
	
	public void union(int p, int q){
		int i = root(p);
		int j = root(q);
		
		id[i] = j;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}

}
