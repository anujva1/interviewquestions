#include <iostream>

using namespace std;

int main(){
	int n = 43;
	int temp = 5;
	bool divisible = false;
	while(n >= temp){
		if(temp == n || temp == n-5){
			cout << "The number is divisible" << endl;
			return 0;
		}else{
			temp = temp + 5;
			n = n-5;
		}
	}

	cout << "The number is not divisible" << endl;
	return 0;
}