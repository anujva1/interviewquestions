package testpackage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import kanzi.filter.seam.*;

public class TestContextResizer {

	/**
	 * @param args
	 */
	private static final String INPUT_FILE_NAME = "C:\\Users\\Anuj_Varma\\Downloads\\shuttle.raw";
	
	public static int byteArrayToInt(byte[] b) 
	{
	    return   b[3] & 0xFF |
	            (b[2] & 0xFF) << 8 |
	            (b[1] & 0xFF) << 16 |
	            (b[0] & 0xFF) << 24;
	}
	
	public static void write(int[] aInput, String aOutputFileName){
	    try {
	      OutputStream output = null;
	      byte[] byteInput = new byte[aInput.length];
	      for(int i=0; i<aInput.length; i++){
	    	  byteInput[i] = (byte)aInput[i];
	      }
	      try {
	        output = new BufferedOutputStream(new FileOutputStream(aOutputFileName));
	        output.write(byteInput);
	      }
	      finally {
	        output.close();
	      }
	    }
	    catch(FileNotFoundException ex){
	    	
	    }
	    catch(IOException ex){
	    	
	    }
	  }
	
	private static int[] readAndClose(InputStream aInput){
	    //carries the data from input to output :    
	    byte[] bucket = new byte[32*1024]; 
	    ByteArrayOutputStream result = null; 
	    try  {
	      try {
	        //Use buffering? No. Buffering avoids costly access to disk or network;
	        //buffering to an in-memory stream makes no sense.
	        result = new ByteArrayOutputStream(bucket.length);
	        int bytesRead = 0;
	        while(bytesRead != -1){
	          //aInput.read() returns -1, 0, or more :
	          bytesRead = aInput.read(bucket);
	          if(bytesRead > 0){
	            result.write(bucket, 0, bytesRead);
	          }
	        }
	      }
	      finally {
	        aInput.close();
	        //result.close(); this is a no-operation for ByteArrayOutputStream
	      }
	    }
	    catch (IOException ex){
	    	
	    }
	    byte[] bytearray = result.toByteArray();
	    int[] image = new int[bytearray.length];
	    for(int i=0; i<bytearray.length; i++){
	    	image[i] = (int)bytearray[i];
	    }
	    return image;
	  }
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		int width = 256;
		int height = 256;
		int direction = ContextResizer.VERTICAL;
		int action = ContextResizer.SHRINK;
		int [] image = null;
		File file = new File(INPUT_FILE_NAME);
		long startTime = System.currentTimeMillis();
		
		int nbGeodesics = width*20/100;
		ContextResizer cr = new ContextResizer(width, height, 0, width, direction, action, width, nbGeodesics);
		try {
	      InputStream input =  new BufferedInputStream(new FileInputStream(file));
	      image = readAndClose(input);
	    }
	    catch (FileNotFoundException ex){
	      ex.printStackTrace();
	    }
		
		Geodesic[] geodesics = cr.computeGeodesics(image, ContextResizer.VERTICAL);
		
		for(int i=0; i<geodesics.length; i++){
			for(int j=0; j<geodesics[i].positions.length; j++){
				//System.out.println("The value of "+i+"th geodesic at position "+j+" : "+geodesics[i].positions[j]+" the cost is: "+geodesics[i].cost);
				image[j*width+geodesics[i].positions[j]] = 255;
			}
		}
		
		int [] image_shrt = new int[image.length];
		//cr.removeGeodesics(geodesics, image, image_shrt, ContextResizer.VERTICAL);
		
		write(image, "output.raw");
		long endTime   = System.currentTimeMillis();
		long totalTime = endTime - startTime;
		System.out.println(totalTime);
	}

}
