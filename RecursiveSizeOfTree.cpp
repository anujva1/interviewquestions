#include <iostream>
#include <string>
#include <vector>

using namespace std;

class Tree{
private:
	class TreeNode{
	public:
		TreeNode *left;
		TreeNode *right;
		int data;

		TreeNode(int d){
			data = d;
			left = NULL;
			right = NULL;
		}
	};

public:
	TreeNode *root;

	Tree(){
		root = NULL;
	}

	Tree(int d){
		root = new TreeNode(d);
	}

	void insertIntoTree(int d){
		if(root == NULL){
			root = new TreeNode(d);
			return;
		}

		TreeNode *temp = root;
		TreeNode *parent = temp;
		while(temp != NULL){
			if(d < temp->data){
				parent = temp;
				temp = temp->left;
			}else{
				parent = temp;
				temp = temp->right;
			}
		}

		//work with the parent;
		if(d < parent->data){
			parent->left = new TreeNode(d);
		}else{
			parent->right = new TreeNode(d);
		}
	}

	void deleteNodeFromTree(int d){

	}

	void printAllNodes(TreeNode *current){
		if(current != NULL){
			cout << current->data << " ";
			printAllNodes(current->left);
			printAllNodes(current->right);
		}else{
			return;
		}
	}

	int sizeOfTree(TreeNode *current){
		if(current != NULL){
			return 1+sizeOfTree(current->left)+sizeOfTree(current->right);
		}else{
			return 0;
		}
	}

	bool twoTreesIdentical(TreeNode *current1, TreeNode *current2){
		if(current1 != NULL && current2 != NULL){
			if(current1->data == current2->data){
				return twoTreesIdentical(current1->left, current2->left) && twoTreesIdentical(current1->right, current2->right);
			}else{
				return false;
			}
		}else if(current1 == NULL && current2 == NULL){
			return true;
		}else{
			return false;
		}
	}

	bool twoTreesIsoMorphic(TreeNode *current1, TreeNode *current2){
		if(current1 != NULL && current2 != NULL){
			return twoTreesIsoMorphic(current1->left, current2->right) && twoTreesIsoMorphic(current1->right, current2->left);
		}else if(current1 == NULL && current2 == NULL){
			return true;
		}else{
			return false;
		}
	}

	int  printAncestors(TreeNode* Tree, int d){
		if( Tree!= NULL &&Tree->data == d){
			cout << Tree->data << "  ";
			return 1;
		}else if(Tree == NULL){
			return 0;
		}else{
			int retleft = printAncestors(Tree->left, d);
			int retright = printAncestors(Tree->right, d);
			if(retleft == 1 || retright == 1){
				cout << Tree->data << " ";
				return 1;
			}
			return 0;
		}
	}

	void printAncestorsWithoutRecursion(TreeNode *current){
		vector<TreeNode *> stackOfVisitedNodes;
		vector<TreeNode *> ancestorList;
		stackOfVisitedNodes.push_back(current);

		while(stackOfVisitedNodes.size() != 0){
			TreeNode *topOfStack = ancestorList[ancestorList.size() - 1];
			if(notVisited(topOfStack->left)){
				stackOfVisitedNodes.push_back(topOfStack->left);
				continue;
			}

			if(notVisited(topOfStack->right)){
				stackOfVisitedNodes.push_back(topOfStack->right);
			}
		}
	}
};

int main(){
	Tree T, T2;
	T.insertIntoTree(5);
	T.insertIntoTree(6);
	T.insertIntoTree(4);
	T.insertIntoTree(7);
	T.insertIntoTree(3);
	T.insertIntoTree(2);
	T.insertIntoTree(8);
	T.insertIntoTree(9);
	T.insertIntoTree(1);
	T2.insertIntoTree(5);
	T2.insertIntoTree(6);
	T2.insertIntoTree(4);
	T2.insertIntoTree(7);
	T2.insertIntoTree(3);
	T2.insertIntoTree(2);
	T2.insertIntoTree(8);
	T2.insertIntoTree(10);
	T2.insertIntoTree(1);
	cout << "The size of this tree is : " << T.sizeOfTree(T.root) << endl;
	cout << "Are the two trees  identical? : " << (T.twoTreesIdentical(T.root, T2.root))? "true":"false";
	cout << endl;
	cout << "Are the two trees  isomorphic? : " << (T.twoTreesIsoMorphic(T.root, T2.root))? "true":"false";
	cout << endl;
	T.printAncestors(T.root, 5);
	cout << endl;
}