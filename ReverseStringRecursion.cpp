#include <iostream>
#include <string>

using namespace std;


void reverse(string str, int index){
	char ch = str[index];
	if(index < str.size()){
		reverse(str, index+1);
	}else{
		return;
	}

	cout << ch;
}

int main(){
	string str = string("abcde");
	reverse(str, 0);
}