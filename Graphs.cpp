#include <iostream>
#include <vector>

class Graph{
private:
	class GraphNode{
	public:
		int data;
		vector<GraphNode *> connectedNodes; 
	};
};