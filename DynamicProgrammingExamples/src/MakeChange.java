/**
 * 
 */

/**
 * @author Anuj_Varma
 *
 */
public class MakeChange {

	/**
	 * @param args
	 */
	
	private int[] coins_available;
	public MakeChange() {
		// TODO Auto-generated constructor stub
		coins_available = new int[6];
		coins_available[0] = 1;
		coins_available[1] = 5;
		coins_available[2] = 10;
		coins_available[3] = 20;
		coins_available[4] = 50;
		coins_available[5] = 100;
	}
	
	class Change_Value{
		public int coin_value;
		public int coin_index;
		public int min;
	};
	
	Change_Value minimum(int money_val){
		int diff[] = new int[coins_available.length];
		for(int i=0; i<coins_available.length; i++){
			diff[i] = money_val-coins_available[i];
		}
		
		//choose the least positive value not the least value
		int min= 999999999; int index = 99999999;
		for(int i=0; i<coins_available.length; i++){
			if(diff[i] < 0){
				continue;
			}
			if(min>diff[i]){
				min = diff[i];
				index = i;
			}
		}
//		System.out.println(index);
		Change_Value cv = new Change_Value();
		cv.coin_index = index;
		cv.coin_value = coins_available[index];
		cv.min = min;
		return cv;
	}
	
	int optimum_Change(int money){
		if(money == 0){
			return 0;
		}
		Change_Value cv = minimum(money);
		return optimum_Change(cv.min)+1;
	}
	
	MakeChange(int money){
		coins_available = new int[6];
		coins_available[0] = 1;
		coins_available[1] = 5;
		coins_available[2] = 10;
		coins_available[3] = 20;
		coins_available[4] = 50;
		coins_available[5] = 100;
	}
	
	MakeChange(int money, int[] coins_avail){
		coins_available = new int[coins_avail.length];
		coins_available = coins_avail;
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		MakeChange mc = new MakeChange(100);
		int number_of_coins = mc.optimum_Change(34);
		System.out.println("The optimum number of coins for change: "+number_of_coins);
	}

}
