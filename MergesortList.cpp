#include <iostream>

using namespace std;

void SortRoutine(ListNode* &head, int sizeOfList){
	if(sizeOfList == 0){
		return;
	}else if(sizeOfList == 1){
		return;
	}else if(sizeOfList == 2){
		if(head->data > head->next->data){
			swap(head->data, head->next->data);
		}
	}
}

void MergeSortList(ListNode* &head){
	int length = 0; //find the length of the list and pass it as a prameter to the sorting routine.
	ListNode* temp = head;
	while(temp != NULL){
		temp = temp->next;
		length++;
	}

	//we have found the length we can start with the sorting routine.
	SortRoutine(head, length);
}