#include <iostream>
#include <vector>
#include <cmath>
#include <utility>
#include <algorithm>

using namespace std;

void pairs(vector<int> xpowery){
	//We have to find all pairs such that x ^ y > y ^ x;
	//naive 
	vector< pair<int, int> > answers;
	for(int i=0 ; i < xpowery.size()-1; i++){
		for(int j = i+1; j < xpowery.size(); j++){
			if(pow(xpowery[i], xpowery[j]) > pow(xpowery[j], xpowery[i])){
				answers.push_back(make_pair(xpowery[i], xpowery[j]));
			}else{
				answers.push_back(make_pair(xpowery[j], xpowery[i]));
			}
		}
	}

	//cout the answers ;
	for(int i=0; i<answers.size(); i++){
		cout << answers[i].first << " " << answers[i].second << endl;
	}
}

void moveAllZerosToEnd(vector<int> array){
	int j=-1;
	bool firstTime = true;
	for(int i=0; i<array.size(); i++){
		if(array[i]==0){
			if(firstTime){
				j = i;
				firstTime = false;	
			}
		}else{
			if(j != -1){
				swap(array[i], array[j]);
				j++;
			}
		}
	}

	for(int i=0; i < array.size(); i++){
		cout << array[i] << " ";
	}
	cout << endl;
}

void rotateArray(vector<int> &array, int rotateBy){
	int temp = array[0];
	int temp1;
	int count = 0;
	int i = 0;
	while(count < rotateBy){
		while(i < array.size()){
			temp1 = array[(i+1)%array.size()];
			array[(i+1)%array.size()] = temp;
			temp = temp1;
			i++;
		}
		i = 0;
		count ++;
	}

	for(int i=0; i<array.size(); i++){
		cout << array[i] << " ";
	}
	cout << endl << endl;
}

//binary search for the least element in a rotated array
int binarySearchRotatedArray(vector<int> array){
	int size = array.size();
	int mid = size/2;
	if(array.size() == 1){
		return array[0];
	}else if(array.size() == 2){
		return (array[0] < array[1])? array[0] : array[1];
	}

	if(array[mid] < array[array.size()-1]){
		if(array[mid] < array[mid-1] && array[mid] < array[mid+1]){
			return array[mid];
		}else{
			vector<int> arrayNew;
			for(int i=0; i<mid; i++){
				arrayNew.push_back(array[i]);
			}
			return binarySearchRotatedArray(arrayNew);
		}
	}else{
		if(array[mid] < array[mid] && array[mid] < array[mid+1]){
			return array[mid];
		}else{
			vector<int> arrayNew;
			for(int i=mid+1; i < array.size(); i++){
				arrayNew.push_back(array[i]);
			}
			return binarySearchRotatedArray(arrayNew);   
		}
	}
}


vector<int> increasingSubsequenceOfMaxProduct(vector<int> array){
	int L[array.size()];
	for(int i=0; i < array.size(); i++){
		L[i] = 0;
	}
	L[0] = 1;
	for(int j=1; j<array.size(); j++){
		int max = 0;
		for(int i=0; i < j; i++){ 
			if((max < L[i]) && (array[i] < array[j])){
				max = L[i];
			}
		}
		L[j] = max + 1;
	}

	//trace back to find the elements in the array
	vector<int> numbers;
	for(int i=array.size()-1; i > 0; i--){
		if(L[i] == L[i-1]){
			continue;
		}else{
			numbers.push_back(array[i]);
		}
	}

	for(int i=0; i<array.size(); i++)
		cout << L[i] << endl; 
	return numbers;
}

int maxContiguousSubArray(vector<int> array){
	int *MaxSum = new int[array.size()];
	MaxSum[0] = array[0];
	int maxSoFar = 0;
	for(int i=1; i<array.size(); i++){
		if(MaxSum[i-1] + array[i] > 0){
			MaxSum[i] = MaxSum[i-1]+array[i];
		}else{
			MaxSum[i] = 0;
		}

		if(maxSoFar < MaxSum[i]){
			maxSoFar = MaxSum[i];
		}
	}

	return maxSoFar;
}

int main(){
	int a[] = {6, 7, 8, 9, 2, 3, 9, 10};
	vector <int> arr(a, a+sizeof(a)/sizeof(a[0]));
	//pairs(arr);
	//moveAllZerosToEnd(arr);
	//rotateArray(arr, 2);

	//cout << binarySearchRotatedArray(arr) << endl;

	//vector<int> increasingSequence = increasingSubsequenceOfMaxProduct(arr);
	/*
	for(int i=0; i<increasingSequence.size(); i++){
		cout << increasingSequence[i] << " ";
	}
	cout << endl << endl;*/

	cout << maxContiguousSubArray(arr) << endl;
}