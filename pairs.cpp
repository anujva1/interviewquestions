#include <iostream>
#include <algorithm>
#include <unordered_map>
#include <vector>

using namespace std;

void createMap(unordered_map<int, int> &mapOfData, vector<int> data){
	for(int i=0; i<data.size(); i++){
		mapOfData.insert(make_pair(data[i], data[i]));
	}
}

int main(){
	int arr[] = {-4,-2,0,2,3,4,6,8,10};
	vector<int> data(arr, arr+sizeof(arr)/sizeof(arr[0]));
	unordered_map<int, int> mapOfData;
	createMap(mapOfData, data);
	int count = 0;
	int k = 2;
	for(int i=0; i<data.size(); i++){
		int check1 = data[i]+k;

		//check for check1
		if(mapOfData.find(check1) == mapOfData.end()){
			continue;
		}else{
			count++;
		}
	}

	cout << "The number of pairs are : " << count << endl;
}