#include <iostream>
#include <vector>
#include <algorithm>

using namespace std;

int searchElementRotated(vector<int> array, int searchElement, int first, int last){
	int mid = (first+last)/2;

	if(first > last){
		return -1;
	}

	if(array[mid] == searchElement){
		return mid;
	}else if(array[mid] < array[last]){
		if(searchElement > array[mid] && searchElement <= array[last]){
			return searchElementRotated(array,searchElement, mid+1, last);
		}else{
			searchElementRotated(array, searchElement, first, mid-1);
		}
	}else if(array[mid] > array[last]){
		if(searchElement < array[mid] && searchElement >= array[first]){
			return searchElementRotated(array, searchElement, first, mid-1);
		}else{
			return searchElementRotated(array, searchElement, mid+1, last);
		}
	}
}

int main(){
	int a[] = {6, 3 , 1, 7, 9, 4, 23, 12, 10};
	vector<int> array(a, a+sizeof(a)/sizeof(a[0]));
	sort(array.begin(), array.end());
	vector<int> rotatedArray;
	rotatedArray.resize(array.size());
	int rotateBy = 5;
	for(int i=0; i<array.size(); i++){
		int newIndex = (i+rotateBy)%array.size();
		rotatedArray[newIndex] =  array[i];
	}

	for(int i=0; i < rotatedArray.size(); i++){
		cout << rotatedArray[i] << endl;
	}

	int searchElement = 12;
	cout << endl << endl;
	cout << searchElementRotated(rotatedArray, searchElement, 0, rotatedArray.size()-1) << endl;
}