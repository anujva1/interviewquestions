#include <iostream>
#include <string>
#include <vector>

using namespace std;

vector<int> Merge(vector<int> left, vector<int> right){
	vector<int> mergedArray;
	int i=0, j=0;
	int index = 0;
	mergedArray.resize(left.size()+right.size());

	while(index < left.size()+right.size()){
		if(i >= left.size()){
			for(;j<right.size(); j++)
				mergedArray[index++] = right[j];
			
			return mergedArray;
		}else if(j >= right.size()){
			for(; i<left.size(); i++)
				mergedArray[index++] = left[i];
			
			return mergedArray;
		}

		if(left[i] < right[j]){
			mergedArray[index] = left[i];
			i++;
		}else{
			mergedArray[index] = right[j];
			j++;
		}
		index++;
	}

	return mergedArray;
}

void MergeSort(vector<int> &arr){
	int mid = arr.size()/2;
	
	if(arr.size() <= 1){
		return;
	}else if(arr.size() == 2){
		if(arr[0] < arr[1]){
			//do nothing
		}else{
			swap(arr[0], arr[1]);
		}
		return;
	}
	
	vector<int> left;
	vector<int> right;

	for(int i=0; i<mid; i++){
		left.push_back(arr[i]);
	}

	for(int i=mid; i<arr.size(); i++){
		right.push_back(arr[i]);
	}

	MergeSort(left);
	MergeSort(right);

	vector<int> mergedSortedArray = Merge(left, right);

	for(int i=0; i<arr.size(); i++){
		cout << mergedSortedArray[i] << " " ;
		arr[i] = mergedSortedArray[i];
	}
	cout << endl;
	return;
}

int main(){
	int a[] = {6, 4, 3, 2, 7, 8, 9, 1, 5};
	vector<int> arr(a, a+sizeof(a)/sizeof(a[0]));

	for(int i=0; i<arr.size(); i++){
		cout << arr[i] << endl;
	}
	cout << endl << endl;

	MergeSort(arr);
	
	cout << endl << endl;

	for(int i=0; i<arr.size(); i++){
		cout << arr[i] << endl;
	}
}