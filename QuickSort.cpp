#include <iostream>
#include <vector>

using namespace std;

void QuickSort(vector<int> &arr, int low, int high){
	//exit case for recursion
	//cout << low << " " << high << endl;
	if(low >= high){
		return;
	}

	//ChoosePivot
	//lets keep the pivot as the first element in the current array.. no need to implement this routine.. yet

	//Partition
	int j=low;
	for(int i=low; i<=high; i++){
		if(arr[i] < arr[low]){
			swap(arr[j+1], arr[i]);
			j++;
		}
	}

	//everything to the left of the pivot is less and everything to the right is more.
	swap(arr[low], arr[j]);

	//Recurse
	QuickSort(arr, low, j-1);
	QuickSort(arr, j+1, high);
}

int main(){
	int a[] = {6, 4, 3, 2, 7, 8, 9, 1, 5};
	vector<int> arr(a, a+sizeof(a)/sizeof(a[0]));

	QuickSort(arr, 0, arr.size()-1);

	for(int i=0; i<arr.size(); i++){
		cout << arr[i] << " ";
	}
	cout << endl;
}