#include <iostream>

using namespace std;

void functionName(char (*a)[4]){
	*a = "abcd";
	cout << *a << endl
;}

int main(){
	char a[] = "xyz";
	functionName(&a);
	cout << a <<endl;
}