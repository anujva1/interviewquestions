#include <iostream>
#include <string>

using namespace std;

void PermuteString(string str, int index){
	if(index == str.size()){
		cout << str << endl;
		return;
	}

	for(int i=index; i<str.size(); i++){
		swap(str[index], str[i]);
		PermuteString(str, index+1);
		//swap(str[index], str[i]);
	}
}

int main(){
	string str = "ABC";
	PermuteString(str, 0);
}