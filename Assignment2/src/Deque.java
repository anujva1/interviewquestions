import java.util.Iterator;


public class Deque<Item> implements Iterable<Item> {

	/**
	 * @param args
	 */
	
	private Node first = null;
	private Node last = null;
	private int size = 0;
	
	private class Node{
		public Item item;
		public Node next;
	}
	
	private class DequeIterator implements Iterator<Item>{
		private int i = 0;
		private Node iteratorNode;
		
		DequeIterator(Deque<Item> deque){
			iteratorNode = deque.first;
		}
		
		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			if( iteratorNode == null ){
				return false;
			}else if(iteratorNode.next == null && i != 0){
				return false;
			}else{
				return true;
			}
		}

		@Override
		public Item next() {
			// TODO Auto-generated method stub
			if(i != 0){
				iteratorNode = iteratorNode.next;
			}
			i++;
			return iteratorNode.item;
		}

		@Override
		public void remove() {
			// TODO Auto-generated method stub
			
		}
	}
	
	public Deque(){
		first = null;
		last = null;
		Node temp = new Node();
		first = last = temp;
		size = 0;
	}
	
	public boolean isEmpty(){
		if(size == 0){
			return true;
		}
		return false;
	}
	
	public int size(){
		return size;
	}
	
	public void addFirst(Item item){
		if(item == null){
			throw new NullPointerException();
		}
		
		if(size == 0){
			first.item = item;
			first.next = null;
			last = first;
		}else{
			Node temp = first;
			first = new Node();
			first.item = item;
			first.next = temp;	
		}
		size++;
	}
	
	public void addLast(Item item){
		if(item == null){
			throw new NullPointerException();
		}
		if(size ==0){
			first.item = item;
			first.next = null;
			last = first;
		}else{
			Node temp = last;
			last = new Node();
			last.item = item;
			temp.next = last;
			last.next = null;
		}
		size++;
	}
	
	public Item removeFirst(){
		if(isEmpty()){
			throw new UnsupportedOperationException();
		}
		
		Node temp = first;
		first = first.next;
		size--;
		return temp.item;
	}
	
	public Item removeLast(){
		if(isEmpty()){
			throw new UnsupportedOperationException();
		}
		Node temp = last;
		Node temp1 = first;
		while(temp1.next != last){
			temp1 = temp1.next;
		}
		last = temp1;
		size--;
		return temp.item;
	}

	@Override
	public Iterator<Item> iterator() {
		// TODO Auto-generated method stub
		return new DequeIterator(this);
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Deque<Integer> deque = new Deque<Integer>();
		deque.addFirst(12);
		deque.addLast(13);
		deque.addFirst(14);
		deque.addFirst(15);
		deque.addLast(11);
		deque.addFirst(10);
		deque.addFirst(16);
		deque.addLast(17);
		deque.addFirst(18);
		deque.addFirst(19);
		deque.addLast(20);
		deque.addFirst(21);
		deque.addFirst(22);
		deque.addLast(23);
		deque.addFirst(24);
		try {
			deque.removeFirst();
			deque.removeFirst();
			deque.removeFirst();
			deque.removeFirst();
			deque.removeFirst();
			deque.removeFirst();
			deque.removeFirst();
			deque.removeFirst();
			deque.removeFirst();
			deque.removeFirst();
			deque.removeFirst();
			deque.removeFirst();
			deque.removeFirst();
			deque.removeFirst();
			deque.removeFirst();
			deque.removeFirst();
			deque.removeFirst();
		} catch (UnsupportedOperationException e) {
			// TODO Auto-generated catch block
			System.out.println("The deque container is empty!");
		}
		Iterator<Integer> itr = deque.iterator();
		while(itr.hasNext()){
			System.out.println(itr.next());
		}
	}
}
