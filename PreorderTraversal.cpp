#include <iostream>

using namespace std;

class TreeNode{
public:
	int data;
	TreeNode *left;
	TreeNode *right;

	TreeNode(){
		left = NULL;
		right = NULL;
	}

	TreeNode(int d){
		data = d;
		left = NULL;
		right = NULL;
	}
};

class Tree{
public:

	TreeNode *root;
	
	Tree(){
		root = NULL;
	}

	Tree(int d){
		root = new TreeNode(d);
	}

	void insertNode(int d){
		if(root == NULL){
			root = new TreeNode(d);
			return;
		}

		TreeNode *temp = root;
		TreeNode *tempParent;
		while (temp != NULL){
			if(d < temp->data){
				tempParent = temp;
				temp  = temp->left;
			}else{
				tempParent = temp;
				temp = temp->right;
			}
		}

		if(tempParent->data > d){
			tempParent->left = new TreeNode(d);
		}else{
			tempParent->right = new TreeNode(d);
		}
	}

	void printTree(TreeNode *node){
		if(node!=NULL){
			cout << node->data << endl;
		}else{
			return;
		}
		printTree(node->left);
		printTree(node->right);
	}
};

int main(){
	Tree tree;
	tree.insertNode(35);
	tree.insertNode(23);
	tree.insertNode(45);
	tree.insertNode(36);
	tree.insertNode(26);
	tree.printTree(tree.root);
}

