import java.util.*;

public class WordNet {

	/**
	 * @param args
	 */
	private class WordNetNode {
		public int synsetId;
		public String synset;
		public String gloss;
		ArrayList<Integer> hypernymNodes;

		public WordNetNode(int synsetId, String synset, String gloss) {
			this.synsetId = synsetId;
			this.synset = synset;
			this.gloss = gloss;
		}
	}

	private ArrayList<WordNetNode> wnNode;

	public WordNet(String synsets, String hypernyms) {
		In synsetFile = new In(synsets);

		while (synsetFile.hasNextLine()) {
			String line = synsetFile.readLine();
			int synsetId = 0;
			String synset = null;
			String gloss;
			int beginIndex = 0;
			int endIndex = 0;
			int count = 0;
			for (int i = 0; i < line.length(); i++) {
				if (line.charAt(i) == ',' && count == 0) {
					synsetId = Integer.parseInt(line.substring(beginIndex,
							endIndex));
					beginIndex = i + 1;
					count++;
				} else if (line.charAt(i) == ',' && count == 1) {
					synset = line.substring(beginIndex, endIndex);
					beginIndex = i + 1;
					count++;
				}
				endIndex++;
			}
			gloss = line.substring(beginIndex, line.length() - 1);
			wnNode.add(new WordNetNode(synsetId, synset, gloss));
		}
		In hypernymFile = new In(hypernyms);
		while (hypernymFile.hasNextLine()) {
			String line = hypernymFile.readLine();
			int count = 0;
			// this line has the first as the index of the synset and the rest
			// are the hypernyms
			int startIndex = 0, endIndex = 0;
			int synsetId = 0;
			for (int i = 0; i < line.length(); i++) {
				if (line.charAt(i) == ',' && count == 0) {
					// this is the first synsetId
					synsetId = Integer.parseInt(line.substring(startIndex,
							endIndex));
					startIndex = i + 1;
					count++;
				} else if (line.charAt(i) == ',' && count > 0) {
					// these are all the hypernyms
					// by this time the synsetId has been set.. so we need to
					// look into the wnNode for that particular
					// luckily we have the synsetId in increasing order already.
					wnNode.get(synsetId).hypernymNodes.add(Integer
							.parseInt(line.substring(startIndex, endIndex)));
					startIndex = i + 1;
					count++;
				}
				endIndex++;
			}
			// the last one has not been added but the startIndex has been
			// updated..
			wnNode.get(synsetId).hypernymNodes.add(Integer.parseInt(line
					.substring(startIndex, endIndex)));
		}
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
