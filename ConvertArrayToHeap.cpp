#include <iostream>
#include <vector>

using namespace std;
void heapify(vector<int> &data, int index){
	int parentIndex = (index-1)/2;

	if(parentIndex < 0){
		return;
	}

	bool parentLessThanChildren = false;

	while(!parentLessThanChildren){
		if((data[parentIndex] <= data[index])){
			parentLessThanChildren = true;
		}else{
			int temp = data[parentIndex];
			data[parentIndex] = data[index];
			data[index] = temp;
			index = parentIndex;
			parentIndex = (parentIndex-1)/2;
			if(parentIndex < 0){
				parentLessThanChildren = true;
			}
		}
	}
}

void ConvertArrayToHeap(vector<int> &arr){
	for(int i=arr.size()-1 ; i > 0 ; i--){
		heapify(arr, i);
	}
}

int main(){
	int a[] = {10,9,8,7,6,5,4,3,2,1};
	vector<int> arr(a, a+sizeof(a)/sizeof(a[0]));
	ConvertArrayToHeap(arr);
	for(int i=0; i<arr.size(); i++){
		cout << arr[i] << " ";
	}
	cout << endl;
}