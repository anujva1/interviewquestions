/*
Copyright 2011, 2012 Frederic Langlet
Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
you may obtain a copy of the License at

                http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package kanzi.entropy;

import kanzi.EntropyDecoder;
import kanzi.EntropyEncoder;
import kanzi.InputBitStream;
import kanzi.OutputBitStream;


public class EntropyCodecFactory 
{
   public static final byte HUFFMAN_TYPE = 72;
   public static final byte NONE_TYPE    = 78;
   public static final byte FPAQ_TYPE    = 70;
   public static final byte PAQ_TYPE     = 80;
   public static final byte RANGE_TYPE   = 82;
   
   
   public EntropyDecoder newDecoder(InputBitStream ibs, byte entropyType)
   {
      if (ibs == null)
         throw new NullPointerException("Invalid null input bitstream parameter");

      switch (entropyType)
      {
         // Each block is decoded separately
         // Rebuild the entropy decoder to reset block statistics
         case HUFFMAN_TYPE:
            return new HuffmanDecoder(ibs);
         case RANGE_TYPE:
            return new RangeDecoder(ibs);
         case PAQ_TYPE:
            return new BinaryEntropyDecoder(ibs, new PAQPredictor());
         case FPAQ_TYPE:
            return new FPAQEntropyDecoder(ibs, new FPAQPredictor());
         case NONE_TYPE:
            return new NullEntropyDecoder(ibs);
         default:
            throw new IllegalArgumentException("Unsupported entropy decoder type: " + (char) entropyType);
      }
   } 
   
   
   public EntropyEncoder newEncoder(OutputBitStream obs, byte entropyType)
   {
      if (obs == null)
         throw new NullPointerException("Invalid null output bitstream parameter");

      switch (entropyType)
      {
         case HUFFMAN_TYPE:
            return new HuffmanEncoder(obs);
         case RANGE_TYPE:
            return new RangeEncoder(obs);
         case PAQ_TYPE:
            return new BinaryEntropyEncoder(obs, new PAQPredictor());
         case FPAQ_TYPE:
            return new FPAQEntropyEncoder(obs, new FPAQPredictor());
         case NONE_TYPE:
            return new NullEntropyEncoder(obs);
         default :
            throw new IllegalArgumentException("Invalid entropy encoder type: " + (char) entropyType);
      }
   }
}
