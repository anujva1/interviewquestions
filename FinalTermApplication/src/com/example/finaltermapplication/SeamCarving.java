package com.example.finaltermapplication;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Arrays;

import kanzi.filter.seam.ContextResizer;
import android.app.ActionBar;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

public class SeamCarving extends Activity {
	
	private Bitmap photo;
	private final static int MINPCT = 10;
    private final static int MAXPCT = 90;
    protected static final String TAG = "kanzi";

    private int pctResize = 0;
    private boolean debug = false;
    private boolean realtime = false;

    private boolean vertical = false;
    private boolean horizontal = false;

    ActionBar ab;
    
    TextView textPctResize;
    ImageView targetImage;
    SeekBar pctBar;
    CheckBox dbgCheck;
    CheckBox rtCheck;
    RadioGroup groupDir;
    RadioButton radioVert;
    RadioButton radioHori;
    RadioButton radioBoth;
	
	
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
	    super.onCreate(savedInstanceState);
	    
	    // TODO Auto-generated method stub
	    setContentView(R.layout.seamcarvinglayout);
	    try{
        	File root_file = Environment.getExternalStorageDirectory();
        	Toast.makeText(getApplicationContext(), "Root File"+root_file.canRead(), Toast.LENGTH_LONG).show();
        	if(root_file.canRead()){
        		File image_photo = new File(root_file, "image.jpg");
        		photo = BitmapFactory.decodeFile(image_photo.getPath());
        		if(photo == null){
        			System.out.println("photo was null");
        		}
        		System.out.println(photo.toString());
        		System.out.println("reached there");
        		
        		File image_photo1 = new File(root_file, "image4.jpg");
        		FileOutputStream out = new FileOutputStream(image_photo1);
        		
        		photo.compress(Bitmap.CompressFormat.JPEG, 100, out);
        		
        		out.flush();
        		out.close();
        	}
        }catch(Exception e){
        	System.out.println("Was not able to read the file");
        }
	    
	    
	    targetImage = (ImageView) findViewById(R.id.target_image);
	    textPctResize = (TextView) findViewById(R.id.pct_elab_text);
	    dbgCheck = (CheckBox) findViewById(R.id.dbg_mode_cb);
        rtCheck = (CheckBox) findViewById(R.id.rt_mode_cb);
        pctBar = (SeekBar) findViewById(R.id.pct_elab_bar);
        groupDir = (RadioGroup) findViewById(R.id.group_dir);
        radioVert = (RadioButton) findViewById(R.id.radio_vert);
        radioHori = (RadioButton) findViewById(R.id.radio_hori);
        radioBoth = (RadioButton) findViewById(R.id.radio_both);

        pctBar.setMax(MAXPCT - MINPCT);
        dbgCheck.setChecked(debug);
        rtCheck.setChecked(realtime);
        radioVert.setChecked(false);
        radioHori.setChecked(false);
        radioBoth.setChecked(false);
        targetImage.setImageBitmap(photo);
        groupDir.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                    switch (checkedId) {
                    case -1:
                            Log.v(TAG, "Huh?");
                            break;
                    case R.id.radio_vert:
                            Log.v(TAG, "vertical");
                            vertical = true;
                            horizontal = false;
                            radioVert.setChecked(true);
                            radioHori.setChecked(false);
                            radioBoth.setChecked(false);
                            break;
                    case R.id.radio_hori:
                            Log.v(TAG, "horizontal");
                            vertical = false;
                            horizontal = true;
                            radioVert.setChecked(false);
                            radioHori.setChecked(true);
                            radioBoth.setChecked(false);
                            break;
                    case R.id.radio_both:
                            Log.v(TAG, "both");
                            vertical = true;
                            horizontal = true;
                            radioVert.setChecked(false);
                            radioHori.setChecked(false);
                            radioBoth.setChecked(true);
                            break;
                    default:
                            Log.v(TAG, "Huh?");
                            break;
                    }
            }
    });
    
    dbgCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                            boolean isChecked) {
                    debug = isChecked;
            }
    });
    
    rtCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton buttonView,
                            boolean isChecked) {
                    realtime = isChecked;
            }
    });

    pctBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {

            }

            @Override
            public void onProgressChanged(SeekBar seekBar, int progress,
                            boolean fromUser) {
                    pctResize = MINPCT + progress;
                    textPctResize.setText("Ridimensiona del " + pctResize + "%");
                    
                    //realtime
                    if(realtime){
                            if (photo == null) {
                                    Toast.makeText(getApplicationContext(),
                                                    "Please load an image!", Toast.LENGTH_SHORT).show();
                            } else if (pctResize == 0) {
                                    Toast.makeText(getApplicationContext(),
                                                    "Percentage is 0!", Toast.LENGTH_SHORT).show();
                            } else if (!vertical && !horizontal) {
                                    Toast.makeText(getApplicationContext(),
                                                    "Choose a Direction!", Toast.LENGTH_SHORT).show();
                            } else {
                                    //Toast.makeText(getApplicationContext(), "Elaborazione..", Toast.LENGTH_SHORT).show();
                                    Bitmap bitmap = kanziResize(photo);
                                    targetImage.setImageBitmap(bitmap);
                            }
                    }
            }
    });

	}
	
	protected Bitmap kanziResize(Bitmap bmp) {

        int w = bmp.getWidth();
        int h = bmp.getHeight();

        int[] src = new int[w * h];
        int[] dst = new int[w * h];

        bmp = bmp.copy(Config.RGB_565, false);//serve?? eccome!! mongolfiere.jpg non funziona altrimenti
        
        bmp.getPixels(src, 0, w, 0, 0, w, h);

        ContextResizer effect = null;

        Arrays.fill(dst, 0);

        //int newW = w;
        int newH = h;
        int difW = 0;
        int difH = 0;

        if (vertical == true) {
                difW = w * pctResize / 100;
                //newW = w - difW;
        }
        if (horizontal == true) {
                difH = h * pctResize / 100;
                newH = h - difH;
        }
        
        if (vertical && !horizontal) {
                effect = new ContextResizer(w, h, 0, w, ContextResizer.VERTICAL,
                                ContextResizer.SHRINK, w, difW);
        }
        if (horizontal) {
                if (vertical) {
                        effect = new ContextResizer(w, h, 0, w,
                                        ContextResizer.VERTICAL, ContextResizer.SHRINK, w, difW);
                        effect.setDebug(debug);
                        effect.apply(src, dst);
                        src = dst;
                        effect = null;
                        effect = new ContextResizer(w, h, 0, w,
                                        ContextResizer.HORIZONTAL, ContextResizer.SHRINK, h,
                                        difH);
                } else {
                        effect = new ContextResizer(w, h, 0, w,
                                        ContextResizer.HORIZONTAL, ContextResizer.SHRINK, h,
                                        difH);
                }
        }
        effect.setDebug(debug);
        effect.apply(src, dst);

        /*
        
        int iter = 1;
        
        long after = 0;
        long before = 0;
        long sum = 0;
        
        for (int ii = 0; ii < iter; ii++) {
                before = System.nanoTime();

                if (vertical && !horizontal) {
                        effect = new ContextResizer(w, h, 0, w, ContextResizer.VERTICAL,
                                        ContextResizer.SHRINK, w, difW);
                }
                if (horizontal) {
                        if (vertical) {
                                effect = new ContextResizer(w, h, 0, w,
                                                ContextResizer.VERTICAL, ContextResizer.SHRINK, w, difW);
                                effect.setDebug(debug);
                                effect.apply(src, dst);
                                src = dst;
                                effect = null;
                                effect = new ContextResizer(w, h, 0, w,
                                                ContextResizer.HORIZONTAL, ContextResizer.SHRINK, h,
                                                difH);
                        } else {
                                effect = new ContextResizer(w, h, 0, w,
                                                ContextResizer.HORIZONTAL, ContextResizer.SHRINK, h,
                                                difH);
                        }
                }
                effect.setDebug(debug);
                effect.apply(src, dst);

                after = System.nanoTime();
                sum += (after - before);

        }
        
        */

        //Log.i("Speedtest","elapsed [ms]: " + sum / 1000000 + " (" + iter + " iterations)");
        
        if (horizontal && vertical && !debug) {
                int goodPixel = newH;
                for (int i = (goodPixel * w); i < dst.length; i++) {
                        dst[i] = 0;
                }
        }
        Bitmap resizedBitmap = Bitmap.createBitmap(dst, w, h, Config.RGB_565);

        return resizedBitmap;
	}

}
