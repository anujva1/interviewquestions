#include <iostream>
#include <vector>
#include <deque>

using namespace std;

class Tree{
private:
	class TreeNode{
	public:
		int data;
		TreeNode *left;
		TreeNode *right;

		TreeNode(int d){
			data = d;
			left = NULL;
			right = NULL;
		}
	};

public:
	TreeNode *root;

	Tree(){
		root = NULL;
	}

	Tree(int d){
		root = new TreeNode(d);
	}

	void insertIntoTree(int d){
		if(root == NULL){
			root = new TreeNode(d);
			return;
		}

		TreeNode *temp = root;
		
		deque<TreeNode *> seenNodes;
		seenNodes.push_back(temp);
		while(temp != NULL){
			temp = seenNodes[0];
			seenNodes.pop_front();

			if(temp->left == NULL){
				temp->left = new TreeNode(d);
				temp = NULL;
			}else if(temp->right == NULL){
				temp->right = new TreeNode(d);
				temp = NULL;
			}else{
				seenNodes.push_back(temp->left);
				seenNodes.push_back(temp->right);
			}
		}

		return;
	}

	void printAllNodes(TreeNode *current){
		if(current == NULL){
			return;
		}

		
		printAllNodes(current->left);
		cout << current->data << " ";
		printAllNodes(current->right);
	}

	void sumAllPathsToLeaves(TreeNode *current, int data, vector<int> &numbers){
		
		int newData;

		if(current->left == NULL && current->right == NULL){
			newData = data * 10 + current->data;
			numbers.push_back(newData);
			return;
		}

		newData = data * 10 + current->data;
		if(current->left != NULL)
			sumAllPathsToLeaves(current->left, newData, numbers);
		if(current->right != NULL)
			sumAllPathsToLeaves(current->right, newData, numbers);
	}

	void printSumOfNumbers(){
		vector<int> numbers;
		sumAllPathsToLeaves(root, 0, numbers);
		for(int i=0; i<numbers.size(); i++){
			cout << numbers[i] << " ";
		}
		cout << endl;
	}

	TreeNode *BuildTree(int pre[], int in[], int insize){
		if(insize == 0){
			return NULL;
		}

		int index;
		int count = 0;
		for(int i=0; i<insize; i++){
			if(pre[0] == in[i]){
				index = i;
				break;
			}
			count ++;
		}

		TreeNode *node = new TreeNode(pre[0]);
		node->left = BuildTree(pre+1, in, count);
		node->right = BuildTree(pre+index+1, in+index+1, insize-count-1);
		return node;
	}
};

int main(){
	Tree t;
	t.insertIntoTree(6);
	t.insertIntoTree(3);
	t.insertIntoTree(5);
	t.insertIntoTree(2);
	t.insertIntoTree(5);
	t.insertIntoTree(4);

	t.printAllNodes(t.root);

	int in[] = {4,2,5,1,3,6};
	int pre[] = {1,2,4,5,3,6};

	Tree t2;
	t2.root = t2.BuildTree(pre, in, 6);

	t2.printAllNodes(t2.root);
}