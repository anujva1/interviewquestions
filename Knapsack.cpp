#include <iostream>
#include <vector>

using namespace std;

class Item{
public:
	int weight;
	int value;

	Item(int w, int v){
		weight = w;
		value = v;
	}
};

int main(){
	vector<Item> items;
	int weights[] = {23, 31, 29, 44, 53, 38, 63, 85, 89, 82};
	int values[] = {92, 57, 49, 68, 60, 43, 67, 84, 87, 72};

	int CapacityOfBag = 165;
	int NumberOfItems = 10;
	vector< vector<int> > M;
	M.resize(CapacityOfBag+1);
	for(int i=0; i<CapacityOfBag+1; i++){
		M[i].resize(NumberOfItems);
	}

	for(int i=0; i<NumberOfItems; i++){
		M[0][i] = 0;
	}

	for(int i=1; i<CapacityOfBag+1; i++){
		for(int j=0; j<NumberOfItems; j++){
			if(i-weights[j] > -1){
				M[i][j] = max(M[i-weights[j]][j-1] + values[j], M[i][j-1]);
			}else{
				M[i][j] = M[i][j-1];
			}
		}
	}

	cout << "The max value that can be achieved is : " << M[CapacityOfBag][NumberOfItems-1] << endl;
	return 0;
}