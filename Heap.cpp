#include <iostream>
#include <vector>

using namespace std;

//array implementation of Heap

template <class T>
class BinaryHeap{
private:
	T *data;
	int size;
	int heapSize;

	void heapify(T *&data, int index){
		int parentIndex = (index-1)/2;

		if(parentIndex < 0){
			return;
		}

		bool parentLessThanChildren = false;

		while(!parentLessThanChildren){
			if((data[parentIndex] <= data[index])){
				parentLessThanChildren = true;
			}else{
				T temp = data[parentIndex];
				data[parentIndex] = data[index];
				data[index] = temp;
				index = parentIndex;
				parentIndex = (parentIndex-1)/2;
				if(parentIndex < 0){
					parentLessThanChildren = true;
				}
			}
		}
	}

public:
	BinaryHeap<T>(){
		data = NULL;
		heapSize = 0;
	}

	BinaryHeap<T>(int size){
		this->size = size;
		data = new T[size];
		heapSize = 0;
	}

	void insertIntoHeap(T d){
		if(heapSize == size){
			cout << "Heap is full, please create some space before inserting" << endl; 
			return;
		}
		data[heapSize++] = d;
		heapify(data, heapSize-1);
	}

	void printHeapData(){
		for(int i=0; i < heapSize; i++){
			cout << data[i] << endl;
		}
	}

};

int main(){
	BinaryHeap<int> heap(10);
	heap.insertIntoHeap(9);
	heap.insertIntoHeap(8);
	heap.insertIntoHeap(7);
	heap.insertIntoHeap(5);
	heap.insertIntoHeap(6);
	heap.insertIntoHeap(4);
	heap.insertIntoHeap(3);
	heap.insertIntoHeap(2);
	heap.insertIntoHeap(1);
	heap.insertIntoHeap(0);
	heap.insertIntoHeap(10);
	heap.printHeapData();

	return 0;
}