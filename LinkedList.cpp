#include <iostream>

using namespace std;

class LinkedList{
private:
	class LinkedListNode{
		public:
			int data;
			LinkedListNode *next;
			LinkedListNode(int data){
				this->data = data;
				this->next = NULL;
			}
	};

public:
	LinkedListNode *head;
	LinkedList(){
		head = NULL;
	}

	LinkedList(int d){
		head = new LinkedListNode(d);
	}

	void insertToHead(int d){
		if(head == NULL){
			head = new LinkedListNode(d);
			return;
		}

		LinkedListNode *temp = head;
		head = new LinkedListNode(d);
		head->next = temp;
	}

	void printAllNodes(){
		LinkedListNode *temp = head;
		while(temp != NULL){
			cout << temp->data << " ";
			temp =  temp->next;
		}
	}

	void reverseList(){
		LinkedListNode *prevtemp;
		LinkedListNode *temp;
		LinkedListNode *nexttemp;

		temp = head;
		nexttemp = temp->next;
		prevtemp = NULL;

		while(temp != NULL){
			nexttemp = temp->next;
			temp->next = prevtemp;
			prevtemp = temp;
			temp = nexttemp;
		}

		head = prevtemp;
	}

	void addTwoListsRepNumbers(LinkedList l1, LinkedList l2){

	}
};

int main(){
	LinkedList l;
	l.insertToHead(5);
	l.insertToHead(6);
	l.insertToHead(7);
	l.insertToHead(8);
	l.insertToHead(9);
	l.reverseList();
	l.printAllNodes();
}