package quickfind;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.Reader;
import java.util.Scanner;

public class QuickFindUF {

	/**
	 * @param args
	 */
	
	private int[] id;
	
	public QuickFindUF(int N) {
		// TODO Auto-generated method stub
		id = new int[N];
		for(int i=0; i<N; i++){
			id[i] = i;
		}
	}
	
	public boolean connected(int p, int q){
		if(id[p] == id[q])
			return true;
		else
			return false;
	}
	
	public void union(int p, int q){
		for(int i=0; i<id.length; i++){
			if(id[p] == id[i]){
				id[i] = id[q];
			}
		}
		id[p] = id[q];
	}
	
	public static boolean checkNumbers(String str){
		//convert the string to number to see if it converts.
		try{
			Integer newint = new Integer(str);
			return true;
		}catch(Exception e){
			return false;
		}
	}
	
	public static void main(String[] args) {
		QuickFindUF qF = new QuickFindUF(10);
		Scanner scanner = new Scanner(System.in);
		System.out.println("Please enter the first node:");
		boolean flag = false,still_adding = true;
		String input1 = null, input2 = null;
		while(still_adding){
			while(!flag){
				input1 = scanner.nextLine();
				if(!checkNumbers(input1)){
					System.out.println("Number entered is not a number please try again....");
				}else{
					flag = true;
				}
			}
			flag = false;
			System.out.println("Please enter the node which you want to connect the node to:");
			while(!flag){
				input2 = scanner.nextLine();
				if(!checkNumbers(input2)){
					System.out.println("Number entered is not a number please try again....");
				}else{
					flag = true;
				}
			}
			flag = false;
			System.out.println("");
			System.out.println(input1+"    "+input2);
			
			//convert the input1 and input2 to integers
			//string to integer conversion
			int node1 = Integer.parseInt(input1);
			int node2 = Integer.parseInt(input2);
			
			if(qF.connected(node1, node2)){
				System.out.println("already connected components.. don't try connecting already connected components :) Try Again?");
				String input3 = scanner.nextLine();
				if(!input3.equalsIgnoreCase("Y")){
					//end this program
					still_adding = false;
				}
			}else{
				qF.union(node1, node2);
				System.out.println("Do you want to add more components?");
				String input4 = scanner.nextLine();
				if(input4.equalsIgnoreCase("Y")){
					//continue;
				}else{
					still_adding = false;
				}
			}
		}
		
		//print bye
		System.out.println("BYE!!");
	}
}
